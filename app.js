const cardsContainer = document.querySelector('.cards-container')
const startButton = document.getElementById('start')
const restartButton = document.getElementById('restart')
const timeSpan = document.getElementById('time')
const movesSpan = document.getElementById('moves')



let previousCard = ''
let matchedCardCount = 0
let cardsOpen = 0

let setTimeInterval = undefined

startButton.addEventListener('click', () => {

    startButton.classList.remove('active')
    restartButton.classList.add('active')
    if (setTimeInterval === undefined) {

        setTimeInterval = setInterval(() => {
            timeSpan.textContent = parseInt(timeSpan.textContent) + 1

        }, 1000);
    }


})

restartButton.addEventListener("click", () => {
    startButton.className = 'start active'
    if (restartButton.classList.contains('active')) {
       
        resetTheGame(cardsContainer)
        clearInterval(setTimeInterval)
        setTimeInterval = undefined
        timeSpan.textContent = 0
        movesSpan.textContent = 0
    }
    restartButton.className = 'restart'
})


cardsContainer.addEventListener('click', (event) => {

    console.log(startButton.className );

    if (
        startButton.className === 'start' &&
        event.target.classList.contains('matched') === false &&
        event.target.classList.contains('card') &&
        cardsOpen < 2
    ) {

        if (previousCard == '') {
            cardsOpen += 1
            previousCard = event.target
            flipCard(event.target)

            movesSpan.textContent = parseInt(movesSpan.textContent) + 1
        }
        else if (checkCardsMatch(event.target, previousCard) == false) {

            cardsOpen += 1
            flipCard(event.target)

            movesSpan.textContent = parseInt(movesSpan.textContent) + 1


            setTimeout(() => {
                cardsOpen = 0
                returnCardOriginalPosition(event.target)

                returnCardOriginalPosition(previousCard)


                previousCard = ''
            }, 500)




        }
        else {
            previousCard = ''
            cardsOpen = 0
        }
    }

})


function checkCardsMatch(currentCard, previousCard) {
    const currentCardNumber = currentCard.classList[1]
    const currentCardFruiteName = currentCard.classList[2]
    const previousCardNumber = previousCard.classList[1]
    const previousCardFruiteName = previousCard.classList[2]

    if (currentCardNumber !== previousCardNumber && previousCardFruiteName === currentCardFruiteName) {


        currentCard.classList.add('matched')

        flipCard(currentCard)


        previousCard.classList.add('matched')
        flipCard(previousCard)


        matchedCardCount += 1


        if (matchedCardCount === 8) {
            startButton.className = 'start'
            clearInterval(setTimeInterval)
            setTimeInterval = undefined

        }

        return true
    }
    else {

        return false
    }

}

function flipCard(card) {
    card.classList.add('rotate')
    card.children[0].classList.add('rotate')
    card.children[0].style.display = 'block'
}

function returnCardOriginalPosition(card) {
    card.classList.remove('rotate')
    card.children[0].classList.remove('rotate')
    card.children[0].style.display = 'none'
}

function resetTheGame(cardsContainer) {
    for (let index = 0; index < cardsContainer.children.length; index++) {


        cardsContainer.children[index].classList.remove('rotate')
        cardsContainer.children[index].classList.remove('rotate-back')
        cardsContainer.children[index].classList.remove('matched')
        cardsContainer.children[index].children[0].style.display = 'none'

    }
}